import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CorpoComponent } from './componentes/corpo/corpo.component';
import { CabecaComponent } from './componentes/cabeca/cabeca.component';
import { DiretivasComponent } from './componentes/diretivas/diretivas.component';

@NgModule({
  declarations: [
    AppComponent,
    CorpoComponent,
    CabecaComponent,
    DiretivasComponent
  ],

  imports: [
    BrowserModule,
    FormsModule   
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
