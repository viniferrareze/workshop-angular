import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-corpo',
  templateUrl: './corpo.component.html',
  styleUrls: ['./corpo.component.css']
})

export class CorpoComponent implements OnInit {
   public tituloCorpo : string; 
   public a : number = 1; 
   public b : number = 2;
   public variavel1 : string;
   public nome : String;

   constructor() { }

   ngOnInit() {
      this.tituloCorpo = "WORKSHOP PRECISA ANGULAR";
      this.variavel1   = "Juca Bala";
   }

   public validaDados(){     
      alert("TESTE OK");
   }

   public motraValor(){
      alert(this.nome);
   }
}
